//
//  MenuViewController.swift
//  Remembery
//
//  Created by Яков on 22.12.2021.
//

import UIKit

class MenuViewController: UIViewController {

    weak var delegate: GameViewControllerDelegate?
    
    @IBOutlet weak var currentLevelLabel: UILabel!
    var selectedLevel: Level? = nil
    
    deinit { print("\n🏁 - \(classForCoder)\n") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presentationController?.delegate = self
        isModalInPresentation = true
        
        showCurrentLevel()
    }
    
    @IBAction func toEasyLevelAction(_ sender: Any) {
        dismiss(animated: true, completion: { self.delegate?.goToLevel(.easy) })
    }
    
    @IBAction func toMediumLevelAction(_ sender: Any) {
        dismiss(animated: true, completion: { self.delegate?.goToLevel(.medium) })
    }
    
    @IBAction func toHardLevelAction(_ sender: Any) {
        dismiss(animated: true, completion: { self.delegate?.goToLevel(.hard) })
    }
    
}

extension MenuViewController {
    
    func showCurrentLevel() {
        var nameLevel: String = ""
        guard let level = selectedLevel else { return }
        
        switch level {
        case .easy:
            nameLevel = "Easy"
        case .medium:
            nameLevel = "Medium"
        case .hard:
            nameLevel = "Hard"
        }
        currentLevelLabel.text = nameLevel
    }
    
}

extension MenuViewController: UIAdaptivePresentationControllerDelegate {
    
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        
        let alertVC = UIAlertController(title: "Continue ?", message: "You have not chosen a level, continue on the previous one?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
            self.dismiss(animated: true, completion: nil)
            guard let level = self.selectedLevel else { return }
            self.delegate?.goToLevel(level)
        })
        alertVC.addAction(okAction)
        present(alertVC, animated: true)
    }
    
}
