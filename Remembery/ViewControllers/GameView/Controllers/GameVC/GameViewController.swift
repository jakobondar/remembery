//
//  GameViewController.swift
//  Remembery
//
//  Created by Яков on 20.12.2021.
//

import UIKit
import SwiftUI

enum Level: Int {
    case easy = 16
    case medium = 24
    case hard = 30
}

//protocol selection of the level of the game
protocol GameViewControllerDelegate: AnyObject {
    func goToLevel(_ selectedLevel: Level)
}

class GameViewController: UIViewController {

    @IBOutlet weak var playOutlet: UIButton!
    @IBOutlet weak var pauseOutlet: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var recordTimeLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var playButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewHeightConstraint: NSLayoutConstraint!
    
    let cardCellID = String(describing: CardCollectionViewCell.self)
    let menuVCID = String(describing: MenuViewController.self)
    let largeConfig = UIImage.SymbolConfiguration(scale: .large)
    
    var recordCounter = 0
    var secondsTens = 0
    var secondsUnit = 0
    var minutes = 0
    var time = Timer()
    var cards = [Card]()
    var numberOpenCards = 0
    var currentTwoCards: [Card] = []
    var playOn: Bool = false
    var pauseOn: Bool = false
    
    var currentLevel: LevelModel = LevelModel(nameLevel: .easy, recordCounter: 0, open: true)
    var tempLevel: LevelModel = LevelModel(nameLevel: .easy, recordCounter: 0, open: true)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playOutlet.tintColor = .white
        pauseOutlet.tintColor = .white
        pauseOutlet.isHidden = true
        collectionView.register(UINib(nibName: cardCellID, bundle: nil), forCellWithReuseIdentifier: cardCellID)
        collViewHeightConstraint.constant = view.frame.width - 40
        
        recordTimeLabel.text = ""
        currentLevel = LevelManager.shared.levels[0]
    }
    
    deinit { print("\n🏁 - \(classForCoder)\n") }

    @IBAction func playAction(_ sender: Any) {
        playOn = true
        pauseOn = false
        showPauseButton()
        setupNewGame()
    }
    
    @IBAction func pauseAction(_ sender: Any) {
        pauseOn = !pauseOn
        pauseOn ? stopTimer() : resumeTimer()
        showPauseButton()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let menuVC = storyboard.instantiateViewController(identifier: menuVCID) as? MenuViewController else { return }
        menuVC.selectedLevel = currentLevel.nameLevel
        menuVC.delegate = self
        stopTimer()
        present(menuVC, animated: true, completion: nil)
    }
    
}
    
extension GameViewController {
    
    //MARK: - pause button
    func showPauseButton() {
        pauseOutlet.isHidden = false
        playButtonConstraint.constant = -45
        
        let anewIcon = UIImage(systemName: "memories", withConfiguration: largeConfig)
        playOutlet.setImage(anewIcon, for: .normal)
        
        let pauseIcon = UIImage(systemName: "pause.fill", withConfiguration: largeConfig)
        let playIcon = UIImage(systemName: "play.fill", withConfiguration: largeConfig)
        pauseOutlet.setImage(pauseOn ? playIcon : pauseIcon, for: .normal)
    }
    
    func hidePauseButton() {
        pauseOutlet.isHidden = true
        playButtonConstraint.constant = 0
        
        let playIcon = UIImage(systemName: "play.fill", withConfiguration: largeConfig)
        playOutlet.setImage(playIcon, for: .normal)
    }
    
    //MARK: - editing constrains collectionView
    func alignGridView() {
        switch currentLevel.nameLevel {
        case .easy: collViewHeightConstraint.constant = view.frame.width - 40
        case .medium: collViewHeightConstraint.constant = (view.frame.width - 40) / 4 * 6
        case .hard: collViewHeightConstraint.constant = (view.frame.width - 40) / 5 * 6
        }
    }
    
}

