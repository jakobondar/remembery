//
//  GameVC+Extensions.swift
//  Remembery
//
//  Created by Яков on 29.12.2021.
//

import Foundation
import UIKit

//MARK: - building a playing field
extension GameViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentLevel.nameLevel.rawValue
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var sizeCell: CGFloat = 0
        
        switch currentLevel.nameLevel {
        case .easy: sizeCell = collectionView.frame.width / 4 - 6
        case .medium: sizeCell = collectionView.frame.width / 4 - 6
        case .hard: sizeCell = collectionView.frame.width / 5 - 2
        }
        return CGSize(width: sizeCell, height: sizeCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        var indent: CGFloat = 0
        
        switch currentLevel.nameLevel {
        case .easy: indent = 8
        case .medium: indent = 6
        case .hard: indent = 2
        }
        return indent
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cardCellID, for: indexPath) as! CardCollectionViewCell
        if !cards.isEmpty {
            cards[indexPath.item].index = [indexPath]
            cell.update(cards[indexPath.item])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if playOn && !pauseOn {
            showCard(indexPath)
        }
    }
    
}

extension GameViewController {
    
    //MARK: - method of opening matching cards
    func showCard(_ indexPath: IndexPath) {
   
        if !cards[indexPath.item].unblock && cards[indexPath.item].isHidden {
            cards[indexPath.item].isHidden = false
            collectionView.reloadItems(at: [indexPath])
            
            if currentTwoCards.count < 2 { currentTwoCards.append(cards[indexPath.item]) }
            
            if currentTwoCards.count == 2 && currentTwoCards[0].id == currentTwoCards[1].id && currentTwoCards[0].index != currentTwoCards[1].index {
                for (key, item) in cards.enumerated() {
                    if currentTwoCards[0].index == item.index || currentTwoCards[1].index == item.index { cards[key].unblock = true }
                }
                currentTwoCards.removeAll()
                numberOpenCards += 2
                
                if numberOpenCards == cards.count {
                    recordGame()
                    presentAlertLevelCompleted()
                }
                
            } else if currentTwoCards.count == 2 && currentTwoCards[0].id != currentTwoCards[1].id {
                for (key, item) in cards.enumerated() {
                    
                    if currentTwoCards[0].index == item.index || currentTwoCards[1].index == item.index{
                        cards[key].isHidden = true
                        
                        let delayTime = DispatchTime.now() + 0.5
                        DispatchQueue.main.asyncAfter(deadline: delayTime) {
                            self.collectionView.reloadItems(at: item.index)
                        }
                    }
                }
                currentTwoCards.removeAll()
            }
        }
    }
    
    //MARK: - creating a new game
    func setupNewGame() {
        
        currentTwoCards = []
        numberOpenCards = 0
        tempLevel = .init(nameLevel: currentLevel.nameLevel, recordCounter: 0, open: currentLevel.open)
        stopTimer()
        recordTimeLabel.text = ""
        timerLabel.text = "0:00"
        
        cards.removeAll()
        cards = CardManager.shared.creatingPairs(level: currentLevel.nameLevel).shuffled()
        isHiddenAllCardsAnimation()
        
        if playOn && !pauseOn {
            
            var delayTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.presentAllCardsAnimation()
            }
            
            delayTime = delayTime + 1.8
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.isHiddenAllCardsAnimation()
                self.showRecordTime()
                self.startTimer()
            }
        }
    }
    
    func presentAllCardsAnimation() {
        for (key, _) in self.cards.enumerated() {
            self.cards[key].unblock = true
            self.cards[key].isHidden = true
        }
        self.collectionView.reloadData()
    }
    
    func isHiddenAllCardsAnimation() {
        for (key, _) in self.cards.enumerated() {
            self.cards[key].unblock = false
            self.cards[key].isHidden = true
        }
        self.collectionView.reloadData()
    }
    
    //MARK: - going to the next level
    func upLevel() {
        
        pauseOn = false
        playOn = false
        
        switch currentLevel.nameLevel {
        case .easy:
            LevelManager.shared.openLevel(.medium)
            currentLevel = LevelManager.shared.levels[1]
            hidePauseButton()
            alignGridView()
            setupNewGame()
            
        case .medium:
            LevelManager.shared.openLevel(.hard)
            currentLevel = LevelManager.shared.levels[2]
            hidePauseButton()
            alignGridView()
            setupNewGame()
            
        case .hard: presentAlertMaxLevel()
        }
    }

    //MARK: - timer
    func startTimer() {
        recordCounter = 0
        secondsUnit = 0
        secondsTens = 0
        minutes = 0
        timerLabel.text = "0:00"
        
        time = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(timerAction),
            userInfo: nil,
            repeats: true)
    }
    
    func resumeTimer() {
        time = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(timerAction),
            userInfo: nil,
            repeats: true)
    }
    
    func stopTimer() {
        time.invalidate()
    }
    
    @objc func timerAction() {
        recordCounter += 1
        secondsUnit += 1
        if secondsUnit > 9 {
            secondsUnit = 0
            secondsTens += 1
        }
        
        if secondsTens > 5 {
            secondsTens = 0
            secondsUnit = 0
            minutes += 1
        }
        timerLabel.text = "\(minutes):\(secondsTens)\(secondsUnit)"
    }
    
    //MARK: - record the game
    func recordGame() {
        tempLevel.nameLevel = currentLevel.nameLevel
        tempLevel.open = currentLevel.open
        tempLevel.recordCounter = recordCounter
 
        LevelManager.shared.breakTheRecord(tempLevel)
        currentLevel = LevelManager.shared.levels.filter({ $0.nameLevel == currentLevel.nameLevel }).first ?? currentLevel
        tempLevel = .init(nameLevel: currentLevel.nameLevel, recordCounter: 0, open: currentLevel.open)
    }
    
    func showRecordTime() {
        
        if currentLevel.recordCounter > 0 {
            minutes = currentLevel.recordCounter / 60
            secondsTens = currentLevel.recordCounter % 60 / 10
            secondsTens > 0 ? (secondsUnit = currentLevel.recordCounter % 60 % 10) : (secondsUnit = currentLevel.recordCounter)
            
            recordTimeLabel.text = "record  \(minutes):\(secondsTens)\(secondsUnit)"
        } else {
            recordTimeLabel.text = ""
        }
    }
    
}

//MARK: - Message extension
extension GameViewController {
    
    //MARK: - message level completed
    func presentAlertLevelCompleted() {
        stopTimer()
        let alertVC = UIAlertController(title: "Congratulations!",
                                    message: "You have completed the level!",
                             preferredStyle: .alert)
        
        let continueLevelAction = UIAlertAction(title: "Сontinue", style: .default, handler: { _ in
            self.pauseOn = true
            self.playOn = true
            self.showPauseButton()
            self.setupNewGame()
            
            switch self.currentLevel.nameLevel {
            case .easy: LevelManager.shared.openLevel(.medium)
            case .medium: LevelManager.shared.openLevel(.hard)
            case .hard: debugPrint("max level")
            }
        })
        alertVC.addAction(continueLevelAction)
        
        let nextLevelAction = UIAlertAction(title: "Next", style: .default, handler: { _ in self.upLevel() })
        alertVC.addAction(nextLevelAction)

        self.present(alertVC, animated: true)
    }
    
    //MARK: - message previous level is not open
    func presentAlertNotOpen() {
        stopTimer()
        let alertVC = UIAlertController(title: "Oops..",
                                    message: "The previous level has not been completed yet, shall we pass it?",
                             preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { _ in self.resumeTimer() })
        
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
            self.pauseOn = true
            self.playOn = true
            self.showPauseButton()
            self.setupNewGame()
        })
        alertVC.addAction(okAction)
        alertVC.addAction(cancelAction)
        
        self.present(alertVC, animated: true)
    }
    
    //MARK: - message previous level is not open
    func presentAlertMaxLevel() {
        stopTimer()
        let alertVC = UIAlertController(title: "Congratulations!",
                                    message: "You have completed the hardest level",
                             preferredStyle: .alert)
        
        
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
            self.pauseOn = true
            self.playOn = true
            self.showPauseButton()
            self.setupNewGame()
        })
        alertVC.addAction(okAction)
        
        self.present(alertVC, animated: true)
    }
}

//MARK: - protocol execution GameViewControllerDelegate
extension GameViewController: GameViewControllerDelegate {
    
    func goToLevel(_ selectedLevel: Level) {
        
        if currentLevel.nameLevel != selectedLevel {
            
            if LevelManager.shared.checkOpenLevel(selectedLevel) {
                playOn = false
                pauseOn = false
            }

            switch selectedLevel {
            case .easy:
                currentLevel = LevelManager.shared.levels[0]
                hidePauseButton()
                alignGridView()
                setupNewGame()
                
            case .medium:
                if LevelManager.shared.checkOpenLevel(.medium) {
                    currentLevel = LevelManager.shared.levels[1]
                    hidePauseButton()
                    alignGridView()
                    setupNewGame()
                } else { presentAlertNotOpen() }
                
            case .hard:
                if LevelManager.shared.checkOpenLevel(.hard) {
                    currentLevel = LevelManager.shared.levels[2]
                    hidePauseButton()
                    alignGridView()
                    setupNewGame()
                } else { presentAlertNotOpen() }
            }
        } else {
            if playOn && !pauseOn {
                resumeTimer()
            }
        }
    }
    
}

