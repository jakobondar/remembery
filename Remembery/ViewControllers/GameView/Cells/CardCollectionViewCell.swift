//
//  CardCollectionViewCell.swift
//  Remembery
//
//  Created by Яков on 20.12.2021.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var shirtImageView: UIImageView!
    @IBOutlet weak var cardImageView: UIImageView!
    
    func update(_ card: Card) {
        
        if !card.isHidden || card.unblock {
            cardImageView.image = card.image
            UIView.transition(
                from: shirtImageView,
                to: cardImageView,
                duration: 0.4,
                options: [.transitionFlipFromRight, .showHideTransitionViews],
                completion: { (finished: Bool) -> () in
                })
        } else {
            UIView.transition(
                from: cardImageView,
                to: shirtImageView,
                duration: 0.4,
                options: [.transitionFlipFromRight, .showHideTransitionViews],
                completion: { (finished: Bool) -> () in
                })
        }
    }
    
}
