//
//  LaunchViewController.swift
//  Remembery
//
//  Created by Яков on 22.12.2021.
//

import UIKit

class LaunchViewController: UIViewController {

    @IBOutlet weak var frontScreensaverImage: UIImageView!
    @IBOutlet weak var backScreensaverImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.animate(withDuration: 1.0,
                       delay: 0.7,
                       options: .allowAnimatedContent,
                       animations: { self.frontScreensaverImage.alpha = 0.0 },
                       completion: {_ in self.loadGameVC() })
        
    }

}

extension LaunchViewController {
    
    func loadGameVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let gameVC = storyBoard.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        self.navigationController?.pushViewController(gameVC, animated: false)
    }
}
