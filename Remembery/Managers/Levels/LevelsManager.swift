//
//  LevelsManager.swift
//  Remembery
//
//  Created by Яков on 28.12.2021.
//

import Foundation

class LevelManager {
    
    static let shared = LevelManager()
    var levels = [LevelModel]()
    
    private init() {
        if let levelsDatabase = DatabaseManager.shared.getData() {
            levels = levelsDatabase
        } else {
            levels = setupLevels()
            DatabaseManager.shared.saveData(levels)
        }
    }
    
    func setupLevels() -> [LevelModel] {
        let easy = LevelModel(nameLevel: .easy, recordCounter: 0, open: true)
        let medium = LevelModel(nameLevel: .medium, recordCounter: 0, open: false)
        let hard = LevelModel(nameLevel: .hard, recordCounter: 0, open: false)
        return [easy, medium, hard]
    }
    
    func openLevel(_ level: Level) {
    
        for (key, item) in levels.enumerated() {
            if item.nameLevel == level {
                levels[key].open = true
            }
        }
    }
    
    func checkOpenLevel(_ level: Level) -> Bool {
        for item in levels {
            if level == item.nameLevel {
                return item.open
            }
        }
        return false
    }
    
    func breakTheRecord(_ level: LevelModel) {
        
        for (key, item) in levels.enumerated() {
            if item.nameLevel == level.nameLevel {
                if item.recordCounter > level.recordCounter || item.recordCounter == 0 {
                    levels[key].recordCounter = level.recordCounter
                }
            }
        }
        DatabaseManager.shared.realmDeleteObjects()
        DatabaseManager.shared.saveData(levels)
    }
    
}
