//
//  DatabaseManeger.swift
//  Remembery
//
//  Created by Яков on 28.12.2021.
//

import Foundation
import RealmSwift

class DatabaseManager {

    static let shared = DatabaseManager()
    private let realm = try? Realm()

    // MARK: - Save and get Database Realm
    func saveData(_ levels: [LevelModel]) {
        let data = transformToRecordDatabase(levels)
        do {
            try realm?.write({
                realm?.add(data)
            })
        } catch(let error) { print(error.localizedDescription) }
    }
    
    func getData() -> [LevelModel]? {
        guard let data = realm?.objects(LevelsListDatabase.self).first else { return nil }
        return transformToLevelModel(data)
    }

    // MARK: - Transform to Realm Model (LevelsListDatabase)
    private func transformToRecordDatabase(_ data: [LevelModel]) -> LevelsListDatabase {
        let levels = LevelsListDatabase()
        
        for datum in data {
            let level = LevelDatabase()
            
            switch datum.nameLevel {
            case .easy: level.level = .easy
            case .medium: level.level = .medium
            case .hard: level.level = .hard
            }
            
            level.recordCounter = datum.recordCounter
            level.open = datum.open
            levels.levels.append(level)
        }
        
        return levels
    }

    // MARK: - Transform back to LevelModel
    private func transformToLevelModel(_ data: LevelsListDatabase) -> [LevelModel] {
        var levels: [LevelModel] = []
        
        for datum in data.levels {
            var nameLevel: Level = .easy
            
            switch datum.level {
            case .easy: nameLevel = .easy
            case .medium: nameLevel = .medium
            case .hard: nameLevel = .hard
            }
            
            let level = LevelModel(nameLevel: nameLevel, recordCounter: datum.recordCounter, open: datum.open)
            levels.append(level)
        }
        return levels
    }
    
    //MARK: - clear realm data
    func realmDeleteObjects() {

        do {
            let realm = try Realm()
            let objects = realm.objects(LevelsListDatabase.self)
            
            try! realm.write {
                realm.delete(objects)
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
}

