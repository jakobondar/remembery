//
//  CardsManager.swift
//  Remembery
//
//  Created by Яков on 28.12.2021.
//

import Foundation
import UIKit

class CardManager {
    
    static let shared = CardManager()
    var allCards = [Card]()
    
    private init() {
        allCards = formationCards()
    }
    
    func creatingPairs(level: Level) -> [Card] {
        var cardsList: [Card] = []
        for card in allCards.shuffled() {
            if cardsList.count < level.rawValue {
                cardsList.append(card)
                cardsList.append(card)
            }
            
        }
        return cardsList
    }
    
    private func formationCards() -> [Card] {
        let card1 = Card.init(id: 1, image: UIImage(named: "1"))
        let card2 = Card.init(id: 2, image: UIImage(named: "2"))
        let card3 = Card.init(id: 3, image: UIImage(named: "3"))
        let card4 = Card.init(id: 4, image: UIImage(named: "4"))
        let card5 = Card.init(id: 5, image: UIImage(named: "5"))
        let card6 = Card.init(id: 6, image: UIImage(named: "6"))
        let card7 = Card.init(id: 7, image: UIImage(named: "7"))
        let card8 = Card.init(id: 8, image: UIImage(named: "8"))
        let card9 = Card.init(id: 9, image: UIImage(named: "9"))
        let card10 = Card.init(id: 10, image: UIImage(named: "10"))
        let card11 = Card.init(id: 11, image: UIImage(named: "11"))
        let card12 = Card.init(id: 12, image: UIImage(named: "12"))
        let card13 = Card.init(id: 13, image: UIImage(named: "13"))
        let card14 = Card.init(id: 14, image: UIImage(named: "14"))
        let card15 = Card.init(id: 15, image: UIImage(named: "15"))
        let card16 = Card.init(id: 16, image: UIImage(named: "16"))
        let card17 = Card.init(id: 17, image: UIImage(named: "17"))
        let card18 = Card.init(id: 18, image: UIImage(named: "18"))
        let card19 = Card.init(id: 19, image: UIImage(named: "19"))
        let card20 = Card.init(id: 20, image: UIImage(named: "20"))
        let card21 = Card.init(id: 21, image: UIImage(named: "21"))
        let card22 = Card.init(id: 22, image: UIImage(named: "22"))
        let card23 = Card.init(id: 23, image: UIImage(named: "23"))
        let card24 = Card.init(id: 24, image: UIImage(named: "24"))
        let card25 = Card.init(id: 25, image: UIImage(named: "25"))
        let card26 = Card.init(id: 26, image: UIImage(named: "26"))
        let card27 = Card.init(id: 27, image: UIImage(named: "27"))
        let card28 = Card.init(id: 28, image: UIImage(named: "28"))
        let card29 = Card.init(id: 29, image: UIImage(named: "29"))
        let card30 = Card.init(id: 30, image: UIImage(named: "30"))
        let card31 = Card.init(id: 31, image: UIImage(named: "31"))
        let card32 = Card.init(id: 32, image: UIImage(named: "32"))
        let card33 = Card.init(id: 33, image: UIImage(named: "33"))
        let card34 = Card.init(id: 34, image: UIImage(named: "34"))
        let card35 = Card.init(id: 35, image: UIImage(named: "35"))
        
        return [card1, card2, card3, card4, card5, card6, card7, card8, card9, card10,
                card11, card12, card13, card14, card15, card16, card17, card18, card19,
                card20, card21, card22, card23, card24, card25, card26, card27, card28,
                card29, card30, card31, card32, card33, card34, card35].shuffled()
    }
}
