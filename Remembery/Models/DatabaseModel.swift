//
//  DatabaseModel.swift
//  Remembery
//
//  Created by Яков on 28.12.2021.
//

import Foundation
import RealmSwift

class LevelsListDatabase: Object {
    var levels: List<LevelDatabase> = List<LevelDatabase>()
}

class LevelDatabase: Object {
    @objc dynamic var level: LevelEnumRealm = .easy
    @objc dynamic var recordCounter: Int = 0
    @objc dynamic var open: Bool = false
    
    @objc enum LevelEnumRealm: Int, RealmEnum {
       case easy
       case medium
       case hard
     }
}
