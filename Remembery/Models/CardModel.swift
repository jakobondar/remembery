//
//  CardModel.swift
//  Remembery
//
//  Created by Яков on 20.12.2021.
//

import Foundation
import UIKit

struct Card {
    var id: Int
    var isHidden: Bool = true
    var unblock: Bool = false
    var image: UIImage?
    var index: [IndexPath] = []
}
