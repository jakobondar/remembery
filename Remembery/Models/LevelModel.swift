//
//  LevelModel.swift
//  Remembery
//
//  Created by Яков on 23.12.2021.
//

import Foundation

struct LevelModel {
    var nameLevel: Level
    var recordCounter: Int
    var open: Bool
}

